# Copyright 2017 clair authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM golang:1.10-alpine AS builder

EXPOSE 6060 6061

ARG CLAIR_REPO_TAG
ENV CLAIR_REPO_TAG ${CLAIR_REPO_TAG:-v2.0.9}

ARG KLAR_EXECUTABLE_VERSION
ENV KLAR_EXECUTABLE_VERSION ${KLAR_EXECUTABLE_VERSION:-2.4.0}

ARG KLAR_EXECUTABLE_SHA
ENV KLAR_EXECUTABLE_SHA ${KLAR_EXECUTABLE_SHA:-09764983d4e9a883754b55b16edf5f0be558ab053ad6ee447aca0199ced3d09f}

WORKDIR /go/src/github.com/coreos/clair/

RUN apk add --no-cache --update git rpm xz && \
    git clone --branch $CLAIR_REPO_TAG https://github.com/coreos/clair /go/src/github.com/coreos/clair/ && \
    wget https://github.com/optiopay/klar/releases/download/v${KLAR_EXECUTABLE_VERSION}/klar-${KLAR_EXECUTABLE_VERSION}-linux-amd64 \
      -O /klar && \
    echo "${KLAR_EXECUTABLE_SHA}  /klar" | sha256sum -c && \
    chmod +x /klar && \
    export CLAIR_VERSION=$(git describe --always --tags --dirty) && \
    go install -ldflags "-X github.com/coreos/clair/pkg/version.Version=$CLAIR_VERSION" -v github.com/coreos/clair/cmd/clair && \
    mv /go/bin/clair /clair

COPY container-scanner /container-scanner

FROM node:12.3.1-alpine

# TODO: figure out how to propagate ENV vars from the build stage without duplicating them here
ARG CLAIR_REPO_TAG
ENV CLAIR_REPO_TAG ${CLAIR_REPO_TAG:-v2.0.9}

ARG KLAR_EXECUTABLE_VERSION
ENV KLAR_EXECUTABLE_VERSION ${KLAR_EXECUTABLE_VERSION:-2.4.0}

# clair depends on git, rpm and xz
COPY --from=builder /bin/rpm /bin/
COPY --from=builder /usr/bin/git /usr/bin/xz /usr/bin/

COPY --from=builder /clair /klar /
COPY --from=builder /container-scanner /container-scanner

# ca-certificates are needed in order for clair to pull images from Docker repositories
# Install GNU wget because Busybox wget sometimes fails with "Connection refused" in start.sh.
# supervisord acts as the orchestration layer to start clair as a background process
RUN apk --no-cache --update add ca-certificates wget supervisor

RUN yarn --cwd /container-scanner/converter install --production --frozen-lockfile && yarn cache clean

WORKDIR /

ENTRYPOINT ["/container-scanner/start.sh"]
