#!/usr/bin/env sh
set -e

function infoMsg() {
  echo ""
  echo -e "\033[1;34m$1\033[0m"
}

function noticeMsg() {
  echo ""
  echo -e "\033[1;36m$1\033[0m"
}

function warningMsg() {
  echo ""
  echo -e "\033[1;33m$1\033[0m"
}

function errorMsg() {
  echo ""
  echo -e "\033[1;31m$1\033[0m"
}

function debugMsg() {
  echo ""
  echo -e "\033[0;36m$1\033[0m"
}

# Defining two new variables based on GitLab's CI/CD predefined variables
# https://docs.gitlab.com/ee/ci/variables/#predefined-environment-variables
export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG}
export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA}

# Prior to this, you need to have the Container Registry running for your project and set up a build job
# with at least the following steps:
#
# docker build -t $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA .
# docker push $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA

# default the DOCKER_USER and DOCKER_PASSWORD to the CI user/password so we can access a private
# GitLab registry without requiring the user to modify any settings
export DOCKER_USER=${DOCKER_USER:-$CI_REGISTRY_USER}
export DOCKER_PASSWORD=${DOCKER_PASSWORD:-$CI_REGISTRY_PASSWORD}

# this is necessary to enable the yarn script to output colour
export FORCE_COLOR=1

# only vulnerabilities with the following severity value or higher will be output
export CLAIR_OUTPUT=${CLAIR_OUTPUT:-Unknown}

# if the KUBERNETES_PORT environment variable is set, then we assume this is being run within the context
# of a kubernetes cluster, therefore we can't rely on the `clair-vulnerabilities-db` service alias url
# and must set this explicitly to the localhost address
if [[ -n "$KUBERNETES_PORT" ]]
then
    warningMsg "Detected KUBERNETES_PORT environment variable, using 127.0.0.1 for CLAIR_VULNERABILITIES_DB_URL"
    export CLAIR_VULNERABILITIES_DB_URL="127.0.0.1" ;
fi

# by default, use the `clair-vulnerabilities-db` service alias url
# (https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#available-settings-for-services)
# defined in the job template of Container Scanning (Container-Scanning.gitlab-ci.yml), however,
# it can be useful to override this value against your local postgres instance when testing locally
export CLAIR_VULNERABILITIES_DB_URL=${CLAIR_VULNERABILITIES_DB_URL:-clair-vulnerabilities-db}

# we should always be running klar against the clair server running locally
export CLAIR_ADDR=localhost

# output format used by klar
export FORMAT_OUTPUT=json

mkdir -p /var/log/supervisor

# allow overriding the postgres vulnerabilities database url used by clair
sed -i s/POSTGRES-VULNERABILITIES-DB-URL/${CLAIR_VULNERABILITIES_DB_URL}/g /container-scanner/clair/config.yaml

# workaround for this bug https://gitlab.com/gitlab-org/gitlab-runner/issues/1380
[ -f /ran.txt ] && exit 0 || >/ran.txt
/usr/bin/supervisord -c /container-scanner/supervisord/supervisord.conf

retries=0

noticeMsg "Waiting for Clair daemon to start"

while( ! wget -T 10 -q -O /dev/null http://localhost:6060/v1/namespaces ) do
  warningMsg "Clair daemon not ready, waiting 1 second before retrying. Clair log contents:"
  supervisorctl tail clair
  sleep 1
  if [ $retries -eq 10 ]
  then
    errorMsg " Timeout, aborting."
    exit 1
  fi
  retries=$(($retries+1))
done

noticeMsg "Clair daemon started successfully."

WHITELIST_FILE=${CI_PROJECT_DIR}/clair-whitelist.yml

noticeMsg "Scanning container from registry '${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG}' for vulnerabilities with severity level '${CLAIR_OUTPUT}' or higher with klar '${KLAR_EXECUTABLE_VERSION}' and clair '${CLAIR_REPO_TAG}'"

set +e
# scan the container and output the report using klar
/klar ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG} > ${CI_PROJECT_DIR}/klar-report.json

# Klar process returns if 0 if the number of detected high severity vulnerabilities in an image is less than or equal
# to the CLAIR_THRESHOLD value (default is 0) and 1 if there were more. It will return 2 if an error has prevented the
# image from being analyzed.  We only want to return an error and stop further processing if klar has returned error code 2
if [ $? -eq 2 ]
then
  errorMsg "An error occurred while attempting to scan the container from registry '${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG}', exiting"
  exit 1
fi

set -e

# transform the klar container scanning report and write the results to a file
yarn run --cwd /container-scanner/converter convert -- ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG} \
    ${CI_PROJECT_DIR}/klar-report.json ${WHITELIST_FILE} ${CI_PROJECT_DIR}/gl-container-scanning-report.json
