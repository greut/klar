const chalk = require('chalk');
const fs = require('fs')
const yaml = require('js-yaml')
const {table} = require('table');

const logger = require('./logger')

const descriptionColumnWidth = 62

const vulnerabilityTableHeaders = {
  status: 'STATUS',
  severity: 'CVE SEVERITY',
  packageName: 'PACKAGE NAME',
  packageVersion: 'PACKAGE VERSION',
  cveDescription: 'CVE DESCRIPTION'
}

function vulnerabilitiesAreEmpty(klarVulnerabilitiesJSON) {
  return Object.entries(klarVulnerabilitiesJSON).length === 0 && klarVulnerabilitiesJSON.constructor === Object
}

function sortVulnerabilities(vulnerabilities) {
  return vulnerabilities.sort((a, b) => a.vulnerability.localeCompare(b.vulnerability))
}

function sortUnapproved(unapproved) {
  return unapproved.sort((a, b) => a.localeCompare(b))
}

function getImageVulnerabilities(dockerImageName, whitelistImageVulnerabilities) {
  const imageWithoutVersion = dockerImageName.split(":")[0]

  return whitelistImageVulnerabilities[imageWithoutVersion] || {}
}

function checkForUnapprovedVulnerabilities(dockerImageName, vulnerabilities, whitelist) {
  const unapproved = []
  const imageVulnerabilities = getImageVulnerabilities(dockerImageName, whitelist.images)

  vulnerabilities.forEach(vulnerability => {
    const vulnerabilityCode = vulnerability.vulnerability
    let vulnerable = true

    // Check if the vulnerability exists in the GeneralWhitelist
    if (whitelist.generalWhitelist[vulnerabilityCode] !== undefined) {
      vulnerable = false
    }

    // If not in GeneralWhitelist check if the vulnerability exists in the imageVulnerabilities
    if (vulnerable && imageVulnerabilities && imageVulnerabilities[vulnerabilityCode]) {
      vulnerable = false
    }

    if (vulnerable) {
      unapproved.push(vulnerabilityCode)
    }
  })

  return unapproved
}

function getVulnerabilities(klarVulnerabilitiesString) {
  let klarVulnerabilitiesJSON
  const vulnerabilities = []

  try {
    klarVulnerabilitiesJSON = JSON.parse(klarVulnerabilitiesString)
  } catch(e) {
    throw new Error(`Error encountered while attempting to convert klar vulnerabilities file into JSON: ${e}`)
  }

  if (vulnerabilitiesAreEmpty(klarVulnerabilitiesJSON)) {
    throw new Error(`Error encountered while attempting to parse klar vulnerabilities report file: Expected JSON file to have key: 'Vulnerabilities'`)
  }

  Object.values(klarVulnerabilitiesJSON.Vulnerabilities).forEach(vulnerabilitiesForLevel => {
    vulnerabilitiesForLevel.forEach(vulnerability => {
      const transformedVulnerability = {
        fixedby: vulnerability.FixedBy,
        severity: vulnerability.Severity,
        link: vulnerability.Link,
        description: vulnerability.Description || "",
        namespace: vulnerability.NamespaceName,
        vulnerability: vulnerability.Name,
        featureversion: vulnerability.FeatureVersion,
        featurename: vulnerability.FeatureName
      }

      vulnerabilities.push(transformedVulnerability)
    })
  })

  return vulnerabilities
}

function sortBySeverity(vulnerabilities) {
  return vulnerabilities.sort((a, b) => a.severity.localeCompare(b.severity))
}

function formatStatus(status) {
  // don't return colour codes when running tests, otherwise it makes it difficult
  // to match on non-colour-coded fixture data
  if (process.env.NODE_ENV === 'test') { return status }

  if (status === "Approved") {
    return chalk.cyan.bold(status)
  }
  return chalk.red.bold(status)
}

function printTable(vulnerabilities, unapproved) {
  const tableConfig = {
    columns: {
      4: {
        width: descriptionColumnWidth,
        wrapWord: true
      }
    }
  };

  const outData = [
    [
      vulnerabilityTableHeaders.status, vulnerabilityTableHeaders.severity, vulnerabilityTableHeaders.packageName,
      vulnerabilityTableHeaders.packageVersion, vulnerabilityTableHeaders.cveDescription
    ]
  ]

  vulnerabilities.forEach(vulnerability => {
    let status = "Approved"
    // TODO: store unapproved vulnerabilities in a hash to avoid n^2 lookup here
    unapproved.forEach(unapprovedVulnerability => {
      if (vulnerability.vulnerability === unapprovedVulnerability) {
        status = "Unapproved"
      }
    })

    outData.push(
      [
        formatStatus(status),
        `${vulnerability.severity} ${vulnerability.vulnerability}`,
        vulnerability.featurename,
        vulnerability.featureversion,
        vulnerability.description ? vulnerability.description : vulnerability.link
      ])
  })

  logger.log(table(outData, tableConfig))
}

function reportToConsole(dockerImageName, vulnerabilities, unapprovedVulnerabilities) {
  const numberOfUnapprovedVulnerabilities = unapprovedVulnerabilities.length
  const numberOfVulnerabilities = vulnerabilities.length

  if (numberOfVulnerabilities === 0) {
    logger.info(`Image [${dockerImageName}] contains NO unapproved vulnerabilities`);
    return
  }

  logger.warn(`Image [${dockerImageName}] contains ${numberOfVulnerabilities} total vulnerabilities`)

  const sortedVulnerabilities = sortBySeverity(vulnerabilities)

  if (numberOfUnapprovedVulnerabilities > 0) {
    logger.error(`Image [${dockerImageName}] contains ${numberOfUnapprovedVulnerabilities} unapproved vulnerabilities`)
  } else {
    logger.info(`Image [${dockerImageName}] contains NO unapproved vulnerabilities`);
  }

  printTable(sortedVulnerabilities, unapprovedVulnerabilities)
}

function reportToFile(glContainerScanningReportFile, image, vulnerabilities, unapproved) {
  const report = {
    image,
    vulnerabilities: sortVulnerabilities(vulnerabilities),
    unapproved: sortUnapproved(unapproved)
  }

  try {
    fs.writeFileSync(glContainerScanningReportFile, JSON.stringify(report, null, 2))
  } catch(e) {
    throw new Error(`Error encountered while attempting to write vulnerabilities report to file with path '${glContainerScanningReportFile}': ${e}`)
  }
}

function parseWhitelistFile(whitelistFilePath) {
  const whitelistSettings = { generalWhitelist: {}, images: {} }
  let whitelistYAML
  try {
    whitelistYAML = yaml.safeLoad(fs.readFileSync(whitelistFilePath, 'utf8'))
  } catch(e) {
    if (e.code === 'ENOENT') {
      logger.warn(`Whitelist file with path '${whitelistFilePath}' does not exist, skipping.`)
      return whitelistSettings
    }

    throw new Error(`Error encountered while attempting to read whitelist file with path '${whitelistFilePath}': ${e}`)
  }

  if (whitelistYAML && whitelistYAML.generalwhitelist){
    whitelistSettings.generalWhitelist = whitelistYAML.generalwhitelist
  }

  if (whitelistYAML && whitelistYAML.images) {
    whitelistSettings.images = whitelistYAML.images
  }

  logger.info(`Using whitelist file with path '${whitelistFilePath}'.`)
  return whitelistSettings
}

function ProcessVulnerabilitiesFile(args) {
  if (args.length < 4) {
    throw new Error('Error: Not enough arguments passed. Usage: node main.js [image-file-name] [json-input-file-name] [whitelist-file] [output-file-name]')
  }

  const dockerImageName = args[0]
  const klarContainerScanningReportFile = args[1]
  const whitelistFile = args[2]
  const glContainerScanningReportFile = args[3]

  let klarVulnerabilitiesFileContents
  try {
    klarVulnerabilitiesFileContents = fs.readFileSync(klarContainerScanningReportFile, 'utf8')
  } catch(e) {
    throw new Error(`Error encountered while attempting to read klar vulnerabilities report file with path '${klarContainerScanningReportFile}': ${e}`)
  }

  const whitelistYAML = parseWhitelistFile(whitelistFile)

  const vulnerabilities = getVulnerabilities(klarVulnerabilitiesFileContents)

  const unapprovedVulnerabilities = checkForUnapprovedVulnerabilities(dockerImageName, vulnerabilities, whitelistYAML)

  // report vulnerabilities
  reportToConsole(dockerImageName, vulnerabilities, unapprovedVulnerabilities)
  reportToFile(glContainerScanningReportFile, dockerImageName, vulnerabilities, unapprovedVulnerabilities)
}

module.exports = {
  ProcessVulnerabilitiesFile
}
