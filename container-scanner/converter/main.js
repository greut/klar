const convert = require('./convert')
const logger = require('./logger')

try {
  convert.ProcessVulnerabilitiesFile(process.argv.slice(2));
} catch(e) {
  logger.error(e.message)
  process.exit(1)
}
