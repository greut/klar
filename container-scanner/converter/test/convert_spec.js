/* global describe, it, context, beforeEach, afterEach */
const chai = require('chai')
const fs = require('fs')
const sinon = require('sinon')

const convert = require('../convert.js')
const logger = require('../logger.js')

const { expect } = chai

chai.use(require('chai-fs'));
chai.use(require('sinon-chai'))

const originalReadFileSync = fs.readFileSync

const outputGLContainerScanningReportPath = './test/tmp/gl-container-scanning-report.json'
const klarReportPath = './test/fixtures/klar-report.json'
const whitelistPath = './test/fixtures/clair-whitelist.yml'
const emptyWhitelistPath = 'empty-whitelist-path'
const everythingWhitelistedPath = './test/fixtures/clair-everything-whitelisted.yml'

const expectedGLContainerScanningReportPath = './test/fixtures/gl-container-scanning-report.json'
const expectedWhitelistedGLContainerScanningReportPath = './test/fixtures/whitelisted-gl-container-scanning-report.json'
const expectedEverythingWhitelistedGLContainerScanningReportPath = './test/fixtures/everything-whitelisted-gl-container-scanning-report.json'

const expectedVulnerabilitiesTable = fs.readFileSync('./test/fixtures/non-whitelisted-vulnerabilities-table.txt', 'utf8')
const expectedWhitelistedVulnerabilitiesTable = fs.readFileSync('./test/fixtures/some-whitelisted-vulnerabilities-table.txt', 'utf8')
const expectedEverythingWhitelistedVulnerabilitiesTable = fs.readFileSync('./test/fixtures/all-whitelisted-vulnerabilities-table.txt', 'utf8')

const imageName = 'image-name-goes-here:version'

afterEach(() => {
  try {
    fs.unlinkSync(outputGLContainerScanningReportPath)
  } catch(e) {
    if(e.code !== 'ENOENT') {
      /* eslint-disable-next-line no-console */
      console.error(`Error occurred while attempting to remove temporary file ${outputGLContainerScanningReportPath}: ${e}`)
    }
  }
})

describe('#ProcessVulnerabilitiesFile', () => {
  let sandbox
  let readFileSyncStub

  beforeEach(() => {
    sandbox = sinon.createSandbox()
    sandbox.spy(logger, 'error')
    sandbox.spy(logger, 'warn')
    sandbox.spy(logger, 'info')
    sandbox.spy(logger, 'log')

    readFileSyncStub = sandbox.stub(fs, 'readFileSync').callsFake(originalReadFileSync)
    readFileSyncStub.withArgs(whitelistPath).returns('')
    readFileSyncStub.withArgs(emptyWhitelistPath).returns('')
  })

  afterEach(() => {
    sandbox.restore()
  })

  context('when invalid parameters are provided', () => {
    context('because not enough arguments are passed', () => {
      it('throws a `Not enough arguments passed` error', () => {
        expect(() => convert.ProcessVulnerabilitiesFile([imageName, klarReportPath])).to.throw(/Error: Not enough arguments passed./)
      })
    })

    context('because the klar container scanning report file does not exist', () => {
      it('throws an error complaining about not being able to read the klar vulnerabilities file', () => {
        expect(() => convert.ProcessVulnerabilitiesFile([imageName, 'non-existent-file', emptyWhitelistPath, outputGLContainerScanningReportPath]) )
          .to.throw(/Error encountered while attempting to read klar vulnerabilities report file/)
      })
    })

    context('because the klar container scanning report does not contain JSON', () => {
      it('throws a JSON parse error', () => {
        const fileWithInvalidContentsPath = 'file-with-invalid-format'

        readFileSyncStub.withArgs(fileWithInvalidContentsPath).returns('file contents here')

        expect(() => convert.ProcessVulnerabilitiesFile([imageName, fileWithInvalidContentsPath, emptyWhitelistPath, outputGLContainerScanningReportPath]) )
          .to.throw(/JSON: SyntaxError: Unexpected token/)
      })
    })

    context('because the klar container scanning report does not contain the "Vulnerabilities" key', () => {
      it('throws an error explaining that the JSON file should have the "Vulnerabilities" key', () => {
        const fileWithInvalidJSONPath = 'file-with-invalid-json'

        readFileSyncStub.withArgs(fileWithInvalidJSONPath).returns('{}')

        expect(() => convert.ProcessVulnerabilitiesFile([imageName, fileWithInvalidJSONPath, emptyWhitelistPath, outputGLContainerScanningReportPath]) )
          .to.throw(/Expected JSON file to have key: 'Vulnerabilities'/)
      })
    })
  })

  context('when valid parameters are provided', () => {
    context('and the whitelist file does not exist', () => {
      it('outputs a warning about the whitelist file not existing, but continues processing the vulnerabilities', () => {
        convert.ProcessVulnerabilitiesFile([imageName, klarReportPath, 'non-existent-whitelist-path', outputGLContainerScanningReportPath])
        expect(logger.warn).to.have.been.calledWith(`Whitelist file with path 'non-existent-whitelist-path' does not exist, skipping.`)
        expect(logger.error).to.have.been.calledWithMatch(/Image \[image-name-goes-here:version\] contains 25 unapproved vulnerabilities/)
      })
    })

    context('and the whitelist file exists', () => {
      context('but the contents of the whitelist file are empty', () => {
        it('outputs the number of unapproved vulnerabilities', () => {
          convert.ProcessVulnerabilitiesFile([imageName, klarReportPath, emptyWhitelistPath, outputGLContainerScanningReportPath])
          expect(logger.error).to.have.been.calledWithMatch(/Image \[image-name-goes-here:version\] contains 25 unapproved vulnerabilities/)
        })

        it('produces an output file which matches the expected report', () => {
          convert.ProcessVulnerabilitiesFile([imageName, klarReportPath, emptyWhitelistPath, outputGLContainerScanningReportPath])
          expect(outputGLContainerScanningReportPath).to.be.a.file().and.equal(expectedGLContainerScanningReportPath)
        })

        it('outputs a table of vulnerabilities', () => {
          convert.ProcessVulnerabilitiesFile([imageName, klarReportPath, emptyWhitelistPath, outputGLContainerScanningReportPath])
          expect(logger.log).to.have.been.calledWithMatch(expectedVulnerabilitiesTable)
        })
      })

      context('and the contents of the whitelist file are not empty', () => {
        beforeEach(() => {
          readFileSyncStub.restore()
        })

        it('outputs the number of total and unapproved vulnerabilities', () => {
          convert.ProcessVulnerabilitiesFile([imageName, klarReportPath, whitelistPath, outputGLContainerScanningReportPath])
          expect(logger.warn).to.have.been.calledWithMatch(/Image \[image-name-goes-here:version\] contains 25 total vulnerabilities/)
          expect(logger.error).to.have.been.calledWithMatch(/Image \[image-name-goes-here:version\] contains 22 unapproved vulnerabilities/)
        })

        it('produces an output file which matches the expected report', () => {
          convert.ProcessVulnerabilitiesFile([imageName, klarReportPath, whitelistPath, outputGLContainerScanningReportPath])
          expect(outputGLContainerScanningReportPath).to.be.a.file().and.equal(expectedWhitelistedGLContainerScanningReportPath)
        })

        it('outputs a table of vulnerabilities', () => {
          convert.ProcessVulnerabilitiesFile([imageName, klarReportPath, whitelistPath, outputGLContainerScanningReportPath])
          expect(logger.log).to.have.been.calledWithMatch(expectedWhitelistedVulnerabilitiesTable)
        })

        context('and every vulnerability has been whitelisted', () => {
          it('outputs the number of total and unapproved vulnerabilities', () => {
            convert.ProcessVulnerabilitiesFile([imageName, klarReportPath, everythingWhitelistedPath, outputGLContainerScanningReportPath])
            expect(logger.info).to.have.been.calledWithMatch(/Image \[image-name-goes-here:version\] contains NO unapproved vulnerabilities/)
          })

          it('produces an output file which matches the expected report', () => {
            convert.ProcessVulnerabilitiesFile([imageName, klarReportPath, everythingWhitelistedPath, outputGLContainerScanningReportPath])
            expect(outputGLContainerScanningReportPath).to.be.a.file().and.equal(expectedEverythingWhitelistedGLContainerScanningReportPath)
          })

          it('outputs a table of vulnerabilities', () => {
            convert.ProcessVulnerabilitiesFile([imageName, klarReportPath, everythingWhitelistedPath, outputGLContainerScanningReportPath])
            expect(logger.log).to.have.been.calledWithMatch(expectedEverythingWhitelistedVulnerabilitiesTable)
          })
        })
      })
    })

    context('and the klar vulnerabilities report is empty', () => {
      beforeEach(() => {
        const emptyKlarVulnerabilitiesReportName = 'empty-klar-vulnerabilities-report'

        readFileSyncStub.withArgs(emptyKlarVulnerabilitiesReportName).returns('{"LayerCount":11,"Vulnerabilities":{}}')

        convert.ProcessVulnerabilitiesFile([imageName, emptyKlarVulnerabilitiesReportName, whitelistPath, outputGLContainerScanningReportPath])
      })

      it('outputs a log message explaining there are no unapproved vulnerabilities', () => {
        expect(logger.info).to.have.been.calledWithMatch(/Image \[image-name-goes-here:version\] contains NO unapproved vulnerabilities/)
      })

      it('saves an empty vulnerabilities report file', () => {
        expect(outputGLContainerScanningReportPath).to.be.a.file().with.content(JSON.stringify({
          "image": "image-name-goes-here:version",
          "vulnerabilities": [],
          "unapproved": []
        }, null, 2))
      })
    })
  })
})
